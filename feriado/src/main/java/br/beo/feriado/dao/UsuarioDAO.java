package br.beo.feriado.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import br.beo.feriado.model.Usuario;

public interface UsuarioDAO extends CrudRepository<Usuario,Integer> {

    public Usuario findByRacfAndSenha(String racf, String senha);
    public Usuario findByRacfOrFuncional(String racf,int funcional);

    @Query(value = "Select new Usuario(u.id, u.nome, u.racf) from Usuario u Where u.id = :id")
    public Usuario buscarUsuarioPorId(@Param("id")Integer id);
    
    @Query(value = "Select id, nome, racf From usuario Where perfil = :perfil", nativeQuery = true)
    public Object[] buscarUsuariosPorPerfil(@Param("perfil")Integer id);
}
