package br.beo.feriado.dao;

import org.springframework.data.repository.CrudRepository;

import br.beo.feriado.model.Agencia;

public interface AgenciaDAO extends CrudRepository<Agencia,Integer>{
    
}
