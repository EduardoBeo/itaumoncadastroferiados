package br.beo.feriado.dao;

import java.util.List;

//import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
//import org.springframework.data.repository.query.Param;

import br.beo.feriado.model.Agencia;
import br.beo.feriado.model.Feriado;

public interface FeriadoDAO extends CrudRepository<Feriado, Integer>{
    
    public List<Feriado> findByAgencia(Agencia agencia);
}
