package br.beo.feriado.model;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "itmn330_agencia")
public class Agencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_agencia")
    private int idAgencia;

    @Column(name = "nome_agencia",length = 100, nullable = false )
    private String nomeAgencia;

    @Column(name = "num_agencia", nullable = false)   
    private int numAgencia;

    public int getIdAgencia() {
        return idAgencia;
    }

    public void setIdAgencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }

    public String getNomeAgencia() {
        return nomeAgencia;
    }

    public void setNomeAgencia(String nomeAgencia) {
        this.nomeAgencia = nomeAgencia;
    }

    public int getNumAgencia() {
        return numAgencia;
    }

    public void setNumAgencia(int numAgencia) {
        this.numAgencia = numAgencia;
    }

    @OneToMany(mappedBy = "agencia")
	// 'atributo' do objeto 'Feriado' que deve ser ignorado ao preencher cada 'Feriado'
	@JsonIgnoreProperties("agencia")
	private List<Feriado> feriados;

    public List<Feriado> getFeriados() {
        return feriados;
    }

    public void setFeriados(List<Feriado> feriados) {
        this.feriados = feriados;
    }

    public Agencia() {
    }

    public Agencia(int idAgencia) {
        this.idAgencia = idAgencia;
    }
}
