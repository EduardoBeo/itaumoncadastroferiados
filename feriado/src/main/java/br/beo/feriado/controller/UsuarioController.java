package br.beo.feriado.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.beo.feriado.dao.UsuarioDAO;
import br.beo.feriado.model.Usuario;



@RestController
@CrossOrigin("*")
public class UsuarioController {

    @Autowired
    private UsuarioDAO dao;
    
    @GetMapping("/usuarios")

    public ArrayList<Usuario> getUsuarios(){

        ArrayList<Usuario> listaUser = (ArrayList<Usuario>) dao.findAll();

        return listaUser;



    }
    //Alterado
    @PostMapping("/usuario/login")
    public ResponseEntity<Usuario> login(@RequestBody Usuario user){
        Usuario userEncontrado = dao.findByRacfOrFuncional(user.getRacf(), user.getFuncional());

        if(userEncontrado != null){
            if(userEncontrado.getSenha().equals(user.getSenha())){
                userEncontrado.setSenha("*");
                 return ResponseEntity.ok(userEncontrado);
            }           
        }

        return ResponseEntity.status(403).build();

    }

    @GetMapping("/usuario/id/{id}")
    public ResponseEntity<Usuario> getUsuarioPorId(@PathVariable int id){
        Usuario usuario =dao.buscarUsuarioPorId(id);

        if (usuario != null) {
                return ResponseEntity.ok(usuario);

            
        } else {
            return ResponseEntity.notFound().build();
            
        }
    }
    
    @GetMapping("/usuarios/perfil/{perfil}")
    public ResponseEntity<Object[]> buscarPorPerfil(@PathVariable int perfil){
        Object[]usuarios=dao.buscarUsuariosPorPerfil(perfil);

        if (usuarios != null) {
            return ResponseEntity.ok(usuarios);
            
        } else {
            return ResponseEntity.notFound().build();
            
        }
    }

    @PostMapping("/usuario/novo")

    public ResponseEntity<Usuario> novoUsuario(@RequestBody Usuario user){
        Usuario novoUser =dao.save(user);

       return ResponseEntity.ok(novoUser);
    }

    @PostMapping("/usuario/update")

    public ResponseEntity<Usuario> updateUsuario(@RequestBody Usuario user){
        if (user!= null && user.getId()>0){
            try {
                Usuario userUpdate =dao.save(user);
                 return ResponseEntity.ok(userUpdate);
            
        } catch(Exception e){
    }
}

       return ResponseEntity.status(400).build();
    }
}
