package br.beo.feriado.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import br.beo.feriado.dao.FeriadoDAO;
import br.beo.feriado.model.Agencia;
import br.beo.feriado.model.Feriado;

@RestController
@CrossOrigin("*")
public class FeriadoController {
  
    @Autowired
    private FeriadoDAO dao;


    @GetMapping ("/feriados")

    public ArrayList<Feriado> getFeriados(){

        ArrayList<Feriado> listaFeriado = (ArrayList<Feriado>) dao.findAll();

        return listaFeriado;
    }

    @PostMapping("/feriado/novo")

    public ResponseEntity<Feriado> novoFeriado(@RequestBody Feriado feriado){
        Feriado novoFeriado =dao.save(feriado);

       return ResponseEntity.ok(novoFeriado);
    }

    @GetMapping("/feriado/agencia/{idAgencia}")

    public ResponseEntity<List<Feriado>> buscarPorAGencia(@PathVariable int idAgencia){
        Agencia agencia = new Agencia(idAgencia);
        List<Feriado>feriados=dao.findByAgencia(agencia);

        if (feriados != null) {
            return ResponseEntity.ok(feriados);
            
        } else {
            return ResponseEntity.notFound().build();
            
        }
    }
}
