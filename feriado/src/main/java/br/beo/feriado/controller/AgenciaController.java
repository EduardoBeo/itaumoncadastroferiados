package br.beo.feriado.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import br.beo.feriado.dao.AgenciaDAO;
import br.beo.feriado.model.Agencia;

@RestController
@CrossOrigin("*")
public class AgenciaController {


    @Autowired
    private AgenciaDAO dao;

    @GetMapping ("/agencias")

    public ArrayList<Agencia> getFeriados(){

        ArrayList<Agencia> listaAgencia = (ArrayList<Agencia>) dao.findAll();

        return listaAgencia;
    }
    
}
